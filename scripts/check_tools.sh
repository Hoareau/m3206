if [ "$(dpkg -s git)" != "false" ]; #dpkg -s permet de savoir si un outil est installé et si il est différent de faux alors il est installé
then
	echo "Git est installé"
fi
if [ "$(dpkg -s tmux)" != "false" ];
then
	echo "tmux est installé"
fi

if [ "$(dpkg -s vim)" != "false" ];
then
	echo "vim est installé"
fi

if [ "$(dpkg -s htop)" != "false" ];
then
	echo "htop est installé"
fi

exit 0
